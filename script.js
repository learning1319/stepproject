window.addEventListener('load', ()=>{
// Our services
    const tabs = document.querySelectorAll('.services_menu_item');
const tabContents = document.querySelectorAll('.services_text');


tabs.forEach((tab, index) => {
  tab.addEventListener('click', () => {
    
    const selectedTab = tab.getAttribute('data-tab');

    tabs.forEach(tab => {
      tab.classList.remove('active_tab');
  });

  tabs[index].classList.add('active_tab');

    tabContents.forEach(content => {
      content.classList.remove('active_text');
      if (content.getAttribute('data-text') === selectedTab) {
        content.classList.add('active_text')};
    });
  });
});


// Our amazing work
// load
const loadingImg = document.querySelector(".display_none");
const loadButton = document.querySelector(".load_img");

loadButton.addEventListener("click", () => {
    loadingImg.classList.remove("display_none");
    loadButton.classList.add("display_none");
})

});
//   filter
const worksImg = document.querySelectorAll(".works_img_item");
const filterTabs = document.querySelectorAll(".works_menu_item")

filterTabs.forEach((tab) => {
    tab.addEventListener('click', () => {
      
      const selectedTab = tab.getAttribute('data-tab');
  
      worksImg.forEach(content => {     
        if(selectedTab === "All"){
            content.classList.remove('display_none'); 
        } else {
            content.classList.add('display_none');}
        if (content.getAttribute('data-img') === selectedTab) {
            content.classList.remove('display_none')}
        
      });
    });
  });
//   people_say
const reviewContainer = document.querySelectorAll(".reviewer_container")
const reviewSmallPhoto = document.querySelectorAll(".small_photo")
const reviewPhoto = document.querySelectorAll(".reviewer_photo")

const leftArrow = document.querySelector(".left_arrow");
const rightArrow = document.querySelector(".right_arrow");


  let currentReviewIndex = 0;

  
  function showCurrentReview() {
    for (let i = 0; i < reviewSmallPhoto.length; i++) {

      if (i === currentReviewIndex) {
        reviewContainer[i].classList.remove("display_none");

        reviewSmallPhoto.forEach((smallPhoto) => {
          if(smallPhoto === reviewSmallPhoto[i]){
            smallPhoto.classList.add("active_reviewer")
            
            const selectedPhoto = smallPhoto.getAttribute("data-reviewer");
            getCurrentInfo(reviewContainer, selectedPhoto, "data-info")

            getCurrentInfo(reviewPhoto, selectedPhoto, "data-photo")
            
            
          } else {
            smallPhoto.classList.remove("active_reviewer")
          }
                })
      } 
        
      }
    }
    function getCurrentInfo(arr, identeficator, atribute){
      arr.forEach((container) =>{
             
        if(identeficator === container.getAttribute(atribute)){
          container.classList.remove("display_none")
          
        }
        else{
          container.classList.add("display_none")
       
        }
      })
    }
    

  leftArrow.addEventListener("click", function () {
    currentReviewIndex = (currentReviewIndex - 1 + reviewContainer.length) % reviewContainer.length;
    showCurrentReview();
  });


rightArrow.addEventListener("click", function () {
  currentReviewIndex = (currentReviewIndex + 1) % reviewContainer.length;
  showCurrentReview();
});

showCurrentReview();


reviewSmallPhoto.forEach(photo=>{
 photo.addEventListener("click", function(){

  photo.classList.add("active_reviewer")
            
  const selectedPhoto = photo.getAttribute("data-reviewer");

  getCurrentInfo(reviewContainer, selectedPhoto, "data-info")
  
  getCurrentInfo(reviewPhoto, selectedPhoto, "data-photo")
  
  
  photo.classList.remove("active_reviewer")
})
})





